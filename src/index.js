'use strict'

const spsave = require('spsave').spsave
const request = require('request-promise')

async function getPmToken (config) {
  try {
    const { access_token: token } = await request.post({
      url: config.pm.baseUrl + '/oauth2/token',
      json: true,
      formData: {
        ...config.credentials,
        grant_type: 'password',
        scope: '*',
        client_id: config.pm.clientId,
        client_secret: config.pm.clientSecret
      }
    })

    return token
  } catch (exception) {
    console.error(exception)
  }
}

async function getFiles (app, type, { config, token }) {
  const options = {
    json: true,
    headers: {
      Authorization: 'Bearer ' + token,
      'User-Agent': 'TMS Archiver'
    }
  }

  try {
    const { app_number: appNumber } = await request.get(`${config.pm.baseUrl}/cases/${app}`, options)
    const files = await request.get(`${config.pm.baseUrl}/cases/${app}/${type}-documents`, options)

    return files.map(file => ({
      folder: config.sp.folder + '/' + appNumber,
      fileName: file.app_doc_filename,
      fileContent: getContent(`${config.pm.baseUrl}/cases/${app}/${type}-document/${file.app_doc_uid}/file`, token),
      checkinMessage: 'Auto-save by TMS Archiver'
    }))
  } catch (exception) {
    throw new Error(exception)
  }
}

async function getContent (fileUrl, token) {
  try {
    const content = await request.get(fileUrl, {
      encoding: null,
      headers: {
        Authorization: 'Bearer ' + token,
        'User-Agent': 'TMS Archiver'
      }
    })

    return content
  } catch (error) {
    console.error(error)
  }
}

async function save (document, config, { token }) {
  try {
    const options = {
      siteUrl: config.sp.site,
      notification: true,
      checkin: true,
      checkinType: 1
    }

    document.fileContent = await document.fileContent

    spsave(options, config.credentials, document)
  } catch (error) {
    console.error({ createError: error.message, document })
  }
}

async function pm2sp (app, config) {
  const token = await getPmToken(config)

  const inputs = await getFiles(app, 'input', { config, token })
  const outputs = await getFiles(app, 'output', { config, token })

  inputs.forEach(file => save(file, config, { token }))
  outputs.forEach(file => save(file, config, { token }))
}

module.exports = pm2sp

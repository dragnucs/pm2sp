# PM2SP

Save input and output documents from ProcessMaker into a SharePoint.

## Install

Yarn

``` bash
$ yarn add pm2sp
```

NPM

``` bash
$ npm install pm2sp
```

## Usage

Given an application/case UID, you can save its input and output documents:

``` javascript
const pm2sp = require('pm2js')

const config = {
  pm: {
    clientId: 'client id',
    clientSecret: 'client secret',
    workspace: 'processmaker workspace',
    serverUri: 'serveruri'
  },
  sp: {
    folder: 'sharepoint folder',
    site: 'sharepoint site'
  },
  credentials: {
    // Account should have access both in ProcessMaker and SharePoint
    username: 'account username',
    password: 'account password',
    domain: 'account domain'
  }
}

const archiver = pm2sp('appUid', config)
```
